-- Dumping database structure for alfazza_new
CREATE DATABASE IF NOT EXISTS `alfazza_new` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `alfazza_new`;

-- Dumping structure for table alfazza_new.tb_customer
CREATE TABLE IF NOT EXISTS `tb_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_surveor` int(11) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `umur` int(11) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `penghasilan` int(11) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `t1` varchar(50) DEFAULT NULL COMMENT 'Pernah Kredit di tempat lain?',
  `brg_kredit` varchar(50) DEFAULT NULL COMMENT 'Elektronik / Apa',
  `brg_merk` varchar(50) DEFAULT NULL,
  `brg_type` varchar(50) DEFAULT NULL,
  `t2` varchar(50) DEFAULT NULL COMMENT 'Informasi Al Fazza darimana?',
  `t3` varchar(50) DEFAULT NULL COMMENT 'Alasan Ingin Mengajukan Kredit',
  `file_ktp` varchar(150) DEFAULT NULL,
  `file_kk` varchar(150) DEFAULT NULL,
  `file_akad` varchar(150) DEFAULT NULL COMMENT 'Alasan Ingin Mengajukan Kredit',
  `rek_listrik` varchar(150) DEFAULT NULL,
  `id_card_kantor` varchar(150) DEFAULT NULL COMMENT 'ID Card Dari Perusahaan',
  `file_ktp_penjamin` varchar(150) DEFAULT NULL,
  `no_hp_penjamin` varchar(50) DEFAULT NULL,
  `maps` varchar(250) DEFAULT NULL,
  `catatan_coo` text DEFAULT NULL,
  `status` int(11) DEFAULT 1 COMMENT '0 Nonaktif / 1 Admin / 2 COO / 3 Surveor / 4 COO / 5 CEO  /6 Akad',
  PRIMARY KEY (`id`)
) COMMENT='Ditujukan untuk pendaftar dari luar yang ingin mengajukan kredit';

-- Dumping structure for table alfazza_new.tb_survei
CREATE TABLE IF NOT EXISTS `tb_survei` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) DEFAULT NULL,
  `t1_1` text DEFAULT NULL,
  `t1_2` text DEFAULT NULL,
  `t1_3` text DEFAULT NULL,
  `t1_4` text DEFAULT NULL,
  `t1_5` text DEFAULT NULL,
  `t2_1` text DEFAULT NULL,
  `t2_2` text DEFAULT NULL,
  `t2_3` text DEFAULT NULL,
  `t2_4` text DEFAULT NULL,
  `t2_5` text DEFAULT NULL,
  `t2_6` text DEFAULT NULL,
  `t2_7` text DEFAULT NULL,
  `t3_1` text DEFAULT NULL,
  `t3_2` text DEFAULT NULL,
  `t3_3` text DEFAULT NULL,
  `t3_4` text DEFAULT NULL,
  `t3_5` text DEFAULT NULL,
  `t3_6` text DEFAULT NULL,
  `t3_7` text DEFAULT NULL,
  `t4_1` text DEFAULT NULL,
  `t4_2` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) COMMENT='Jawaban hasil dari pertanyaan surveor';

-- Dumping structure for table alfazza_new.tb_users
CREATE TABLE IF NOT EXISTS `tb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT 0 COMMENT '1 Admin/ 2 COO/ 3 CEO/ 4 surveor',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='Tabel untuk users dan hak akses';

-- Dumping data for table alfazza_new.tb_users: ~5 rows (approximately)
/*!40000 ALTER TABLE `tb_users` DISABLE KEYS */;
INSERT INTO `tb_users` (`id`, `nama_lengkap`, `email`, `username`, `password`, `status`) VALUES
	(1, 'Admin', 'admin@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
	(2, 'Sardi', 'sardi@gmail.com', 'sardi', '04fa8fa4a83332800fec174cc0928521', 3),
	(3, 'Faisal', 'faisal@gmail.com', 'faisal', 'f4668288fdbf9773dd9779d412942905', 2),
	(4, 'Rifal', 'suroveor@gmail.com', 'surveor', 'cf3bc95aafef169fbe06b882aea024ec', 4),
	(5, 'Aryandi', 'aryandi@gmail.com', 'aryandi', '8f44edca00ce59dab0fd270aad526e96', 5);
/*!40000 ALTER TABLE `tb_users` ENABLE KEYS */;

-- Dumping structure for trigger alfazza_new.xttrg_add_customer
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `xttrg_add_customer` AFTER INSERT ON `tb_customer` FOR EACH ROW BEGIN
	INSERT INTO tb_survei SET id_customer = NEW.id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;