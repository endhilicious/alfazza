-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.22-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table alfazza_store.tb_customer
CREATE TABLE IF NOT EXISTS `tb_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `umur` int(11) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `penghasilan` int(11) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `t1` varchar(50) DEFAULT NULL COMMENT 'Pernah Kredit di tempat lain?',
  `brg_kredit` varchar(50) DEFAULT NULL COMMENT 'Elektronik / Apa',
  `brg_merk` varchar(50) DEFAULT NULL,
  `brg_type` varchar(50) DEFAULT NULL,
  `t2` varchar(50) DEFAULT NULL COMMENT 'Informasi Al Fazza darimana?',
  `t3` varchar(50) DEFAULT NULL COMMENT 'Alasan Ingin Mengajukan Kredit',
  `file_ktp` varchar(150) DEFAULT NULL,
  `file_kk` varchar(150) DEFAULT NULL,
  `file_akad` varchar(150) DEFAULT NULL COMMENT 'Alasan Ingin Mengajukan Kredit',
  `rek_listrik` varchar(150) DEFAULT NULL,
  `id_card_kantor` varchar(150) DEFAULT NULL COMMENT 'ID Card Dari Perusahaan',
  `file_ktp_penjamin` varchar(150) DEFAULT NULL,
  `no_hp_penjamin` varchar(50) DEFAULT NULL,
  `maps` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT 0 COMMENT '(0) Untuk Tidak Aktif - (1) Aktif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='Ditujukan untuk pendaftar dari luar yang ingin mengajukan kredit';

-- Dumping data for table alfazza_store.tb_customer: ~5 rows (approximately)
/*!40000 ALTER TABLE `tb_customer` DISABLE KEYS */;
INSERT INTO `tb_customer` (`id`, `nama_lengkap`, `alamat`, `umur`, `pekerjaan`, `penghasilan`, `no_hp`, `nama_perusahaan`, `t1`, `brg_kredit`, `brg_merk`, `brg_type`, `t2`, `t3`, `file_ktp`, `file_kk`, `file_akad`, `rek_listrik`, `id_card_kantor`, `file_ktp_penjamin`, `no_hp_penjamin`, `maps`, `status`) VALUES
	(7, 'MUH. AMRU FAUZI H. K', 'Bumi Permata Sudiang Blok A2 No 14,', 21, 'Freelance Web Developer', 2000000, '+6281247352852', 'Amru Fauzi', 'Tidak', 'asd', 'fdsd', 'dfsf', 'dsfsd', 'sdfsfs', 'KTP_11zon.png', 'logo.png', 'WhatsApp_Image_2022-04-15_at_12_20_25.jpeg', 'Temu_Bakat___Hasil_Strength_Typologi_-_MUH__AMRU_FAUZI_H__KAMARUDDIN,_S_KOM.png', 'tobse-fritz-rpvhOzyv2jM-unsplash.jpg', '1640695993907.jpg', '+6281247352852', 'sfcsdf', 0),
	(8, 'amru Fausii', 'bps', 123, 'Makan', 2000000, '0821232121', 'PT. DEVCAFE', 'Tidak', 'Elektronik', 'Apple', 'Macbook Pro 2020', 'Ya', 'Tidak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'MUH. AMRU FAUZI H. K', 'Bumi Permata Sudiang Blok A2 No 14,', 22, 'Freelance Web Developer', 2000000, '+6281247352852', 'Amru Fauzi', 'Tidak', 'Elektronik', 'Lenovo', 'L3i-24', 'Website', 'Kebutuhan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 'MUH. AMRU FAUZI H. K', 'Bumi Permata Sudiang Blok A2 No 14,', 22, 'Freelance Web Developer', 2000000, '+6281247352852', 'Amru Fauzi', 'Tidak', 'asdad', 'ads', 'asdas', 'Web', 'fg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 'MUH. AMRU FAUZI H. K', 'Bumi Permata Sudiang Blok A2 No 14,', 22, 'Freelance Web Developer', 2000000, '+6281247352852', 'Amru Fauzi', 'qw', 'sad', 'sadsa', 'dsad', 'asda', 'dsa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `tb_customer` ENABLE KEYS */;

-- Dumping structure for table alfazza_store.tb_pengajuan
CREATE TABLE IF NOT EXISTS `tb_pengajuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `umur` int(11) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `penghasilan` int(11) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `t1` varchar(50) DEFAULT NULL COMMENT 'Pernah Kredit di tempat lain?',
  `brg_kredit` varchar(50) DEFAULT NULL COMMENT 'Elektronik / Apa',
  `brg_merk` varchar(50) DEFAULT NULL,
  `brg_type` varchar(50) DEFAULT NULL,
  `t2` varchar(50) DEFAULT NULL COMMENT 'Informasi Al Fazza darimana?',
  `t3` varchar(50) DEFAULT NULL COMMENT 'Alasan Ingin Mengajukan Kredit',
  `status` int(11) DEFAULT 0 COMMENT '(0) Untuk Tidak Aktif - (1) Aktif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2147483648 DEFAULT CHARSET=utf8mb4 COMMENT='Ditujukan untuk pendaftar dari luar yang ingin mengajukan kredit';

-- Dumping data for table alfazza_store.tb_pengajuan: ~1 rows (approximately)
/*!40000 ALTER TABLE `tb_pengajuan` DISABLE KEYS */;
INSERT INTO `tb_pengajuan` (`id`, `nama_lengkap`, `alamat`, `umur`, `pekerjaan`, `penghasilan`, `no_hp`, `nama_perusahaan`, `t1`, `brg_kredit`, `brg_merk`, `brg_type`, `t2`, `t3`, `status`) VALUES
	(0, 'MUH. AMRU FAUZI H. K', 'Bumi Permata Sudiang Blok A2 No 14,', 22, 'Freelance Web Developer', 2000000, '+6281247352852', 'Amru Fauzi', 'Ya', 'Elektronik', 'Lenovo', 'L31-24', 'Website', 'Kebutuhan', NULL);
/*!40000 ALTER TABLE `tb_pengajuan` ENABLE KEYS */;

-- Dumping structure for table alfazza_store.tb_users
CREATE TABLE IF NOT EXISTS `tb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='Tabel untuk users dan hak akses';

-- Dumping data for table alfazza_store.tb_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `tb_users` DISABLE KEYS */;
INSERT INTO `tb_users` (`id`, `nama_lengkap`, `email`, `username`, `password`, `status`) VALUES
	(1, 'Faisal', 'faisal@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
	(2, 'Sardi', 'sardi@gmail.com', 'sardi', '04fa8fa4a83332800fec174cc0928521', 2);
/*!40000 ALTER TABLE `tb_users` ENABLE KEYS */;

-- Dumping structure for trigger alfazza_store.xttrg_customer_after_delete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `xttrg_customer_after_delete` BEFORE DELETE ON `tb_pengajuan` FOR EACH ROW BEGIN
  INSERT INTO tb_customer 
	SELECT 
		NULL AS id,
  		nama_lengkap, 
		alamat, 
		umur,
		pekerjaan, 
		penghasilan,
		no_hp, 
		nama_perusahaan, 
		t1, 
		brg_kredit, 
		brg_merk, 
		brg_type, 
		t2, 
		t3, 
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL FROM tb_pengajuan WHERE id = OLD.id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
