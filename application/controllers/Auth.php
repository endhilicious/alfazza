<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
    }
    public function index()
    {
        if ($this->session->userdata('authenticated')) {
            redirect('Admin'); 
        } else {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu'); 
            $this->load->view('login/login'); 
        }
    }

    public function login()
    {
        $username = trim($this->input->post('username')); 
        $password = md5($this->input->post('password')); 

        if(!preg_match("/^[a-zA-Z0-9]*$/", $username)){
            $this->session->set_flashdata('message', 'Input hanya huruf dan angka yang diijinkan, dan tidak boleh menggunakan spasi!'); 
            redirect('Admin');
        }

        $user = $this->Login_model->get($username); 
        if (empty($user)) { 
            $this->session->set_flashdata('message', 'Username atau Password salah'); 
            redirect('Admin'); 
        } else {
            if ($password == $user->password) { 
                $session = array(
                    'authenticated' => true, 
                    'id' => $user->id, 
                    'username' => $user->username,  
                    'nama' => $user->nama_lengkap, 
                    'status' => $user->status,
                    'image' => $user->file_foto,
                );
                $this->session->set_userdata($session); 
                redirect('Dashboard'); 
            } else {
                $this->session->set_flashdata('message', 'Password salah'); 
                redirect('Admin');
            }
        }
    }
    public function logout()
    {
        $this->session->sess_destroy(); 
        redirect('Admin'); 
    }
}
