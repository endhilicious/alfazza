<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {

    public function __construct()
    {
      parent::__construct();
        if ($this->session->userdata('authenticated') != true) {
                $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
                redirect(site_url('Admin'));
        }
        if ($this->session->userdata('status') != 1) {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
            redirect(site_url('Dashboard'));
        }
        $this->load->database();
        $this->load->model('M_Pengajuan');
        $this->load->helper('url');
    }

	public function index()
	{
    $data['title'] = "Pengajuan - Al Fazza";
    $data['pengajuan'] = $this->M_Pengajuan->getAll();
		$this->load->view('templates/header', $data);
		$this->load->view('admin/pengajuan/index', $data);
		$this->load->view('templates/footer');
	}

	public function add()
	{
        $data['title'] = "Tambah Pengajuan - Al Fazza";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/pengajuan/add');
		$this->load->view('templates/footer');
	}

	public function addData()
	{
        $data = array(
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'alamat' 		=> $this->input->post('alamat'),
            'umur' 			=> $this->input->post('umur'),
            'pekerjaan' 	=> $this->input->post('pekerjaan'),
            'penghasilan' 	=> $this->input->post('penghasilan'),
            'no_hp' 		=> $this->input->post('no_hp'),
            'nama_perusahaan' => $this->input->post('nama_perusahaan'),
            't1' 			=> $this->input->post('t1'),
            'brg_kredit' 	=> $this->input->post('brg_kredit'),
            'brg_merk' 		=> $this->input->post('brg_merk'),
            'brg_type' 		=> $this->input->post('brg_type'),
            't2' 			=> $this->input->post('t2'),
            't3' 			=> $this->input->post('t3')
        );
        
        $this->M_Pengajuan->insert($data);
        redirect(site_url('Pengajuan'));
	}


	public function update($id = null)
	{
        $data['title'] = "Edit Pengajuan - Al Fazza";
		if(!isset($id)) redirect('pengajuan');
        $data['pengajuan'] = $this->M_Pengajuan->getById($id);
		if (!$data['pengajuan']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('admin/pengajuan/update', $data);
		$this->load->view('templates/footer');
	}
	
	public function detail($id = null)
	{
        $data['title'] = "Detail Pengajuan - Al Fazza";
		if(!isset($id)) redirect('pengajuan');
        $data['pengajuan'] = $this->M_Pengajuan->getById($id);
		if (!$data['pengajuan']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('admin/pengajuan/detail', $data);
		$this->load->view('templates/footer');
	}

	public function updateData()
	{
        $ID 	=  $this->input->post('id');
		$data = array(
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'alamat' 		=> $this->input->post('alamat'),
            'umur' 			=> $this->input->post('umur'),
            'pekerjaan' 	=> $this->input->post('pekerjaan'),
            'penghasilan' 	=> $this->input->post('penghasilan'),
            'no_hp' 		=> $this->input->post('no_hp'),
            'nama_perusahaan' => $this->input->post('nama_perusahaan'),
            't1' 			=> $this->input->post('t1'),
            'brg_kredit' 	=> $this->input->post('brg_kredit'),
            'brg_merk' 		=> $this->input->post('brg_merk'),
            'brg_type' 		=> $this->input->post('brg_type'),
            't2' 			=> $this->input->post('t2'),
            't3' 			=> $this->input->post('t3')
        );

		$this->M_Pengajuan->update($data, $ID);
        redirect(site_url('Pengajuan'));

	}

	public function deleteData($id = null){
        $pengajuan = $this->M_Pengajuan;
        $validation = $this->form_validation;
		$pengajuan->delete($id);  
		redirect(site_url('Pengajuan'));
    }

    public function accept($id = null){
        $result = $this->M_Pengajuan->accept($id);
		redirect(site_url('Pengajuan'));
    }

    public function d_accept($id = null){
        $result = $this->M_Pengajuan->d_accept($id);
		redirect(site_url('Pengajuan'));
    }
}
