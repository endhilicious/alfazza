<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survei extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('authenticated') != true) {
			$this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
			redirect(site_url('Admin'));
		}
		if ($this->session->userdata('status') != 4) {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
            redirect(site_url('Dashboard'));
        }
        $this->load->database();
        $this->load->model('M_Survei');
        $this->load->model('M_Customer');
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Customer - Al Fazza";
        $data['customer'] = $this->M_Survei->getAll();
		$this->load->view('templates/header', $data);
		$this->load->view('surveor/survei/index', $data);
		$this->load->view('templates/footer');
	}

	public function addsurvei()
	{	
		$ID 	=  $this->input->post('id');
        $data = array('id_surveor' 	=> $this->input->post('surveor'));
        $this->M_Survei->update($data, $ID);
		$this->M_Survei->accept($ID);
        redirect(site_url('customer'));
	}


	public function update($id = null)
	{
        $data['title'] = "Edit Customer - Al Fazza";
		if(!isset($id)) redirect('customer');
        $data['customer'] = $this->M_Survei->getById($id);
		if (!$data['customer']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('surveor/survei/update', $data);
		$this->load->view('templates/footer');
	}
	
	public function detail($id = null)
	{
        $data['title'] = "Detail Customer - Al Fazza";
		if(!isset($id)) redirect('customer');
        $data['customer'] = $this->M_Customer->getById($id);
        $data['resultsurvei'] = $this->M_Customer->getResultSurvei($id);
		if (!$data['customer']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('surveor/survei/detail', $data);
		$this->load->view('templates/footer');
	}

	public function survei($id = null)
	{
        $data['title'] = "Survei Customer - Al Fazza";
		if(!isset($id)) redirect('survei');
        $data['customer'] = $this->M_Customer->getById($id);
        $data['survei'] = $this->M_Survei->getById($id);
		if (!$data['survei']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('surveor/survei/update', $data);
		$this->load->view('templates/footer');
	}

	public function updateData()
	{

		$file_ktp			= $_FILES['file_ktp'];
		$file_kk			= $_FILES['file_kk'];
		$file_akad			= $_FILES['file_akad'];
		$rek_listrik		= $_FILES['rek_listrik'];
		$id_card_kantor		= $_FILES['id_card_kantor'];
		$file_ktp_penjamin	= $_FILES['file_ktp_penjamin'];

		$ID 	=  $this->input->post('id');
		$data_survei = array(
			't1_1' => $this->input->post('t1_1'),
			't1_2' => $this->input->post('t1_2'),
			't1_3' => $this->input->post('t1_3'),
			't1_4' => $this->input->post('t1_4'),
			't1_5' => $this->input->post('t1_5'),
			't2_1' => $this->input->post('t2_1'),
			't2_2' => $this->input->post('t2_2'),
			// 't2_3' => $this->input->post('t2_3'),
			't2_4' => $this->input->post('t2_4'),
			't2_5' => $this->input->post('t2_5'),
			't2_6' => $this->input->post('t2_6'),
			// 't2_7' => $this->input->post('t2_7'),
			't3_1' => $this->input->post('t3_1'),
			't3_2' => $this->input->post('t3_2'),
			't3_3' => $this->input->post('t3_3'),
			't3_4' => $this->input->post('t3_4'),
			't3_5' => $this->input->post('t3_5'),
			// 't3_6' => $this->input->post('t3_6'),
			't3_7' => $this->input->post('t3_7'),
			't4_1' => $this->input->post('t4_1'),
			't4_2' => $this->input->post('t4_2'),
			'comment' => $this->input->post('comment'),
		);

		$data_lampiran = array(
            'no_hp_penjamin' => $this->input->post('no_hp_penjamin'),
            'maps' 			=> $this->input->post('maps'),
        );

		$config['upload_path'] = 'assets/upload';
		$config['allowed_types'] = 'jpeg|jpg|png|pdf';

		$this->load->library('upload', $config);

		if (!empty($_FILES['file_ktp']['name'])) {
			if (!$this->upload->do_upload('file_ktp')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			} else {
				$data_lampiran['file_ktp']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['file_kk']['name'])) {
			if(!$this->upload->do_upload('file_kk')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data_lampiran['file_kk']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['file_akad']['name'])) {
			if(!$this->upload->do_upload('file_akad')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data_lampiran['file_akad']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['rek_listrik']['name'])) {
			if(!$this->upload->do_upload('rek_listrik')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data_lampiran['rek_listrik']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['id_card_kantor']['name'])) {
			if(!$this->upload->do_upload('id_card_kantor')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data_lampiran['id_card_kantor']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['file_ktp_penjamin']['name'])) {
			if(!$this->upload->do_upload('file_ktp_penjamin')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data_lampiran['file_ktp_penjamin']	= $this->upload->data('file_name');
			}
		}
		$this->M_Customer->update($data_lampiran, $ID);
		$this->M_Survei->update($data_survei, $ID);
        redirect(site_url('Survei'));
	}

	public function deleteData($id = null){
        $customer = $this->M_Survei;
        $validation = $this->form_validation;
		$customer->delete($id);  
		redirect(site_url('Survei'));
    }

	private function _do_upload()
    {
        $image_name = time().'-'.$_FILES["image"]['name'];

        $config['upload_path'] 		= 'assets/upload/images/';
        $config['allowed_types'] 	= 'gif|jpg|png';
        $config['file_name'] 		= $image_name;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
            redirect('');
        }
        return $this->upload->data('file_name');
    }
}
