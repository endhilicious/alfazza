<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('authenticated') != true) {
			$this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
			redirect(site_url('Admin'));
		}
		if ($this->session->userdata('status') != 5) {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
            redirect(site_url('Dashboard'));
        }
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Users - Al Fazza";
		$this->load->view('templates/header', $data);
		$this->load->view('super_admin/profile/index', $data);
		$this->load->view('templates/footer');
	}
}
