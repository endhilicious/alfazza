<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('authenticated') != true) {
			$this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
			redirect(site_url('Admin'));
		}
		if ($this->session->userdata('status') != 2) {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
            redirect(site_url('Dashboard'));
        }
        $this->load->database();
        $this->load->model('M_Customer');
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Customer - Al Fazza";
        $data['customer'] = $this->M_Customer->getAll();
		$this->load->view('templates/header', $data);
		$this->load->view('coo/customer/index', $data);
		$this->load->view('templates/footer');
	}

	public function add()
	{
        $data['title'] = "Tambah Customer - Al Fazza";
		$this->load->view('templates/header', $data);
		$this->load->view('coo/customer/add');
		$this->load->view('templates/footer');
	}

	public function addData()
	{	
		$file_ktp			= $_FILES['file_ktp'];
		$file_kk			= $_FILES['file_kk'];
		$file_akad			= $_FILES['file_akad'];
		$rek_listrik		= $_FILES['rek_listrik'];
		$id_card_kantor		= $_FILES['id_card_kantor'];
		$file_ktp_penjamin	= $_FILES['file_ktp_penjamin'];
		
		if (!empty($_FILES['file_ktp']['name']) 
			&& !empty($_FILES['file_kk']['name'])
			&& !empty($_FILES['file_akad']['name'])
			&& !empty($_FILES['rek_listrik']['name'])
			&& !empty($_FILES['id_card_kantor']['name'])
			&& !empty($_FILES['file_ktp_penjamin']['name'])) 
		{
            $config['upload_path'] = 'assets/upload';
			$config['allowed_types'] = 'jpeg|jpg|png|pdf';

			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('file_ktp')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			} else {
				$file_ktp			= $this->upload->data('file_name');
			}
			if(!$this->upload->do_upload('file_kk')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$file_kk			= $this->upload->data('file_name');
			}
			if(!$this->upload->do_upload('file_akad')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$file_akad			= $this->upload->data('file_name');
			}
			if(!$this->upload->do_upload('rek_listrik')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$rek_listrik		= $this->upload->data('file_name');
			}
			if(!$this->upload->do_upload('id_card_kantor')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$id_card_kantor		= $this->upload->data('file_name');
			}
			if(!$this->upload->do_upload('file_ktp_penjamin')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$file_ktp_penjamin	= $this->upload->data('file_name');
			}
			
        }

        $data = array(
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'alamat' 		=> $this->input->post('alamat'),
            'umur' 			=> $this->input->post('umur'),
            'pekerjaan' 	=> $this->input->post('pekerjaan'),
            'penghasilan' 	=> $this->input->post('penghasilan'),
            'no_hp' 		=> $this->input->post('no_hp'),
            'nama_perusahaan' => $this->input->post('nama_perusahaan'),
            't1' 			=> $this->input->post('t1'),
            'brg_kredit' 	=> $this->input->post('brg_kredit'),
            'brg_merk' 		=> $this->input->post('brg_merk'),
            'brg_type' 		=> $this->input->post('brg_type'),
            't2' 			=> $this->input->post('t2'),
            't3' 			=> $this->input->post('t3'),
            'no_hp_penjamin' => $this->input->post('no_hp_penjamin'),
            'maps' 			=> $this->input->post('maps'),
            'file_ktp' 		=> $file_ktp,
            'file_kk' 		=> $file_kk,
            'file_akad' 	=> $file_akad,
            'rek_listrik' 	=> $rek_listrik,
            'id_card_kantor' 	=> $id_card_kantor,
            'file_ktp_penjamin' => $file_ktp_penjamin
        );
        
        $this->M_Customer->insert($data);
        redirect(site_url('Customer'));
		// var_dump($data);
	}
	public function addsurvei()
	{	
		$ID 	=  $this->input->post('id');
        $data = array('id_surveor' 	=> $this->input->post('surveor'));
        $this->M_Customer->update($data, $ID);
		$this->M_Customer->accept($ID);
        redirect(site_url('Customer'));
	}


	public function update($id = null)
	{
        $data['title'] = "Edit Customer - Al Fazza";
		if(!isset($id)) redirect('customer');
        $data['customer'] = $this->M_Customer->getById($id);
		if (!$data['customer']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('coo/customer/update', $data);
		$this->load->view('templates/footer');
	}
	
	public function detail($id = null)
	{
        $data['title'] = "Detail Customer - Al Fazza";
		if(!isset($id)) redirect('customer');
        $data['customer'] = $this->M_Customer->getById($id);
        $data['surveor'] = $this->M_Customer->getAllSurveor();
		if (!$data['customer']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('coo/customer/detail', $data);
		$this->load->view('templates/footer');
	}

	public function updateData()
	{
        $customer = $this->M_Customer;
        $validation = $this->form_validation;

		$file_ktp			= $_FILES['file_ktp'];
		$file_kk			= $_FILES['file_kk'];
		$file_akad			= $_FILES['file_akad'];
		$rek_listrik		= $_FILES['rek_listrik'];
		$id_card_kantor		= $_FILES['id_card_kantor'];
		$file_ktp_penjamin	= $_FILES['file_ktp_penjamin'];

		$ID 	=  $this->input->post('id');
		$data = array(
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'alamat' 		=> $this->input->post('alamat'),
            'umur' 			=> $this->input->post('umur'),
            'pekerjaan' 	=> $this->input->post('pekerjaan'),
            'penghasilan' 	=> $this->input->post('penghasilan'),
            'no_hp' 		=> $this->input->post('no_hp'),
            'nama_perusahaan' => $this->input->post('nama_perusahaan'),
            't1' 			=> $this->input->post('t1'),
            'brg_kredit' 	=> $this->input->post('brg_kredit'),
            'brg_merk' 		=> $this->input->post('brg_merk'),
            'brg_type' 		=> $this->input->post('brg_type'),
            't2' 			=> $this->input->post('t2'),
            't3' 			=> $this->input->post('t3'),
            'no_hp_penjamin' => $this->input->post('no_hp_penjamin'),
            'maps' 			=> $this->input->post('maps'),
        );

		$config['upload_path'] = 'assets/upload';
		$config['allowed_types'] = 'jpeg|jpg|png|pdf';

		$this->load->library('upload', $config);

		if (!empty($_FILES['file_ktp']['name'])) {
			if (!$this->upload->do_upload('file_ktp')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			} else {
				$data['file_ktp']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['file_kk']['name'])) {
			if(!$this->upload->do_upload('file_kk')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data['file_kk']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['file_akad']['name'])) {
			if(!$this->upload->do_upload('file_akad')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data['file_akad']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['rek_listrik']['name'])) {
			if(!$this->upload->do_upload('rek_listrik')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data['rek_listrik']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['id_card_kantor']['name'])) {
			if(!$this->upload->do_upload('id_card_kantor')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data['id_card_kantor']	= $this->upload->data('file_name');
			}
		}
		if (!empty($_FILES['file_ktp_penjamin']['name'])) {
			if(!$this->upload->do_upload('file_ktp_penjamin')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			}else{
				$data['file_ktp_penjamin']	= $this->upload->data('file_name');
			}
		}
		
		$this->M_Customer->update($data, $ID);
        redirect(site_url('Customer'));
	}

	public function deleteData($id = null){
        $customer = $this->M_Customer;
        $validation = $this->form_validation;
		$customer->delete($id);  
		redirect(site_url('Customer'));
    }

	private function _do_upload()
    {
        $image_name = time().'-'.$_FILES["image"]['name'];

        $config['upload_path'] 		= 'assets/upload/images/';
        $config['allowed_types'] 	= 'gif|jpg|png';
        $config['file_name'] 		= $image_name;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
            redirect('');
        }
        return $this->upload->data('file_name');
    }

	public function accept_not_survey($id = null){
        $data = array('status'	=> 5);
        $this->M_Customer->update($data, $id);
        redirect(site_url('Customer'));
	}

	public function accept($id = null){
        $result = $this->M_Customer->accept($id);
		redirect(site_url('Customer'));
    }

    public function d_accept($id = null){
        $result = $this->M_Customer->d_accept($id);
		redirect(site_url('Customer'));
    }
}
