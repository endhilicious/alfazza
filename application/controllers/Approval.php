<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends CI_Controller {

  public function __construct()
  {
      parent::__construct();
      if ($this->session->userdata('authenticated') != true) {
        $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
        redirect(site_url('Admin'));
      }
      if ($this->session->userdata('status') != 3) {
        $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
        redirect(site_url('Dashboard'));
    }
      $this->load->database();
      $this->load->model('M_Customer');
      $this->load->helper('url');
  }

	public function index()
	{
    $data['title'] = "Survei Customer - Al Fazza";
    $data['approval'] = $this->M_Customer->getAllApproval();
		$this->load->view('templates/header', $data);
		$this->load->view('ceo/approval/index', $data);
		$this->load->view('templates/footer');
	}

    public function detail($id = null)
	{
    $data['title'] = "Detail - Al Fazza";
		if(!isset($id)) redirect('customer');
    $data['approval'] = $this->M_Customer->getById($id);
    $data['resultsurvei'] = $this->M_Customer->getResultSurvei($id);
		if (!$data['approval']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('ceo/approval/detail', $data);
		$this->load->view('templates/footer');
	}

	public function push_to_ceo()
	{
    $ID 	=  $this->input->post('id');
		$data = array(
      'catatan_coo' 	=> $this->input->post('catatan_coo')
    );

		$this->M_Customer->update($data, $ID);
		$this->M_Customer->accept($ID);
        redirect(site_url('Approval'));
	}

  public function accept($id = null){
    $result = $this->M_Customer->accept($id);
    redirect(site_url('Approval'));
  }

  public function d_accept(){
		$ID 	=  $this->input->post('id');
		$data = array(
            'keterangan_reject' 	=> $this->input->post('keterangan_reject')
        );

		$this->M_Customer->update($data, $ID);
		$this->M_Customer->d_accept($ID);
        redirect(site_url('Approval'));
    }
}
