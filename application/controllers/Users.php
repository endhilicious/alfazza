<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('authenticated') != true) {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
            redirect(site_url('Admin'));
        }
        if ($this->session->userdata('status') != 5) {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
            redirect(site_url('Dashboard'));
        }
        $this->load->database();
        $this->load->model('M_Users');
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Users - Al Fazza";
        $data['users'] = $this->M_Users->getAll();
		$this->load->view('templates/header', $data);
		$this->load->view('super_admin/user/index', $data);
		$this->load->view('templates/footer');
	}

    public function add()
	{
        $data['title'] = "Tambah Customer - Al Fazza";
		$this->load->view('templates/header', $data);
		$this->load->view('super_admin/user/add');
		$this->load->view('templates/footer');
	}

    public function addData()
	{	
        $data = array(
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'email' 		=> $this->input->post('email'),
            'username' 		=> $this->input->post('username'),
            'status' 	    => $this->input->post('status'),
            'password' 	    => md5($this->input->post('password')),
        );
        
        $config['upload_path']      = 'assets/upload/users';
		$config['allowed_types']    = 'jpeg|jpg|png|pdf';
        $config['file_name'] 	    = 'profile-'.$this->input->post('username');
		$this->load->library('upload', $config);

		if (!empty($_FILES['file_foto']['name'])) {
			if (!$this->upload->do_upload('file_foto')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			} else {
				$data['file_foto']	= $this->upload->data('file_name');
			}
		}

        $this->M_Users->insert($data);
        redirect(site_url('Users'));
	}

	public function update($id = null)
	{
        $data['title'] = "Edit Users - Al Fazza";
		if(!isset($id)) redirect('Users');
        $data['users'] = $this->M_Users->getById($id);
		if (!$data['users']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('super_admin/user/update', $data);
		$this->load->view('templates/footer');
	}

	public function updateData()
	{
        $customer = $this->M_Users;
        $validation = $this->form_validation;

		$ID 	=  $this->input->post('id');
		$data = array(
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'email' 		=> $this->input->post('email'),
            'username' 		=> $this->input->post('username'),
            'status' 	    => $this->input->post('status'),
        );
		
        if ($this->input->post('password') != null) {
            $data['password'] = md5($this->input->post('password'));
        }
        
        $config['upload_path']      = 'assets/upload/users';
		$config['allowed_types']    = 'jpeg|jpg|png|pdf';
        $config['file_name'] 	    = 'profile-'.$this->input->post('username');
		$this->load->library('upload', $config);

		if (!empty($_FILES['file_foto']['name'])) {
			if (!$this->upload->do_upload('file_foto')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			} else {
				$data['file_foto']	= $this->upload->data('file_name');
			}
		}
		$this->M_Users->update($data, $ID);
        redirect(site_url('Users'));
	}

	public function deleteData($id = null){
        $customer = $this->M_Users;
        $validation = $this->form_validation;
		$customer->delete($id);  
		redirect(site_url('Users'));
    }

    private function _do_upload($username)
    {
        $image_name = time().'-'.$_FILES["image"]['name'];

        $config['upload_path'] 		= 'assets/upload/images/';
        $config['allowed_types'] 	= 'gif|jpg|png';
        $config['file_name'] 		= 'profile-'.$this->session->userdata('username');

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('image')) {
            $this->session->set_flashdata('msg', $this->upload->display_errors('', ''));
            redirect('');
        }
        return $this->upload->data('file_name');
    }

	public function accept($id = null){
        $result = $this->M_Users->accept($id);
		redirect(site_url('Users'));
    }

    public function d_accept($id = null){
        $result = $this->M_Users->d_accept($id);
		redirect(site_url('Users'));
    }
}
