<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('authenticated') != true) {
			$this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
			redirect(site_url('Admin'));
		}
        $this->load->database();
        $this->load->model('M_Dashboard');
	    $this->load->helper('url');
    }

	public function index()
	{
		$data['title'] = "Al Fazza - App";
		$data['all'] = $this->M_Dashboard->getAll();
		$data['customer'] = $this->M_Dashboard->getAllActiveCustomer();
		$data['pengajuan'] = $this->M_Dashboard->getAllPengajuan();
		$data['total_customer'] = $this->M_Dashboard->getAllCustomer();
		$this->load->view('templates/header', $data);
		$this->load->view('dashboard');
		$this->load->view('templates/footer');
	}
}
