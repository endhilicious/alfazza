<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OnSurvei extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('authenticated') != true) {
			$this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
			redirect(site_url('Admin'));
		}
		if ($this->session->userdata('status') != 2) {
            $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
            redirect(site_url('Dashboard'));
        }
        $this->load->database();
        $this->load->model('M_Customer');
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Survei Customer - Al Fazza";
        $data['onsurvei'] = $this->M_Customer->getAllSurvei();
		$this->load->view('templates/header', $data);
		$this->load->view('coo/onsurvei/index', $data);
		$this->load->view('templates/footer');
	}

    public function detail($id = null)
	{
        $data['title'] = "Detail - Al Fazza";
		if(!isset($id)) redirect('customer');
        $data['onsurvei'] = $this->M_Customer->getById($id);
        $data['resultsurvei'] = $this->M_Customer->getResultSurvei($id);
		if (!$data['onsurvei']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('coo/onsurvei/detail', $data);
		$this->load->view('templates/footer');
	}

	public function push_to_ceo()
	{
        $ID 	=  $this->input->post('id');
		$data = array(
            'catatan_coo' 	=> $this->input->post('catatan_coo')
        );

		$this->M_Customer->update($data, $ID);
		$this->M_Customer->accept($ID);
        redirect(site_url('Onsurvei'));
	}

	public function d_accept(){
		$ID 	=  $this->input->post('id');
		$data = array(
            'keterangan_reject' 	=> $this->input->post('keterangan_reject')
        );

		$this->M_Customer->update($data, $ID);
		$this->M_Customer->d_accept($ID);
        redirect(site_url('Onsurvei'));
    }
}
