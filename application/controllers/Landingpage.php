<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landingpage extends CI_Controller {

	public function index()
	{
		$data['title'] = "Al Fazza - App";
		$this->load->view('landingpage/index');
	}
}
