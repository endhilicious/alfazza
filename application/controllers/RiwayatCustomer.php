<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RiwayatCustomer extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('authenticated') != true) {
			$this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
			redirect(site_url('Admin'));
		}
        $this->load->database();
        $this->load->model('M_Customer');
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Customer - Al Fazza";
        $data['customer'] = $this->M_Customer->getAllCustomer();
		$this->load->view('templates/header', $data);
		$this->load->view('coo/riwayatCustomer/index', $data);
		$this->load->view('templates/footer');
	}
	
	public function detail($id = null)
	{
        $data['title'] = "Detail Customer - Al Fazza";
		if(!isset($id)) redirect('customer');
        $data['customer'] = $this->M_Customer->getById($id);
		$data['surveor'] = $this->M_Customer->getSurveorByID($data['customer']->id_surveor);
		$data['resultsurvei'] = $this->M_Customer->getResultSurvei($id);
		if (!$data['customer']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('coo/riwayatCustomer/detail', $data);
		$this->load->view('templates/footer');
	}

	public function deleteData($id = null){
        $customer = $this->M_Customer;
        $validation = $this->form_validation;
		$customer->delete($id);  
		redirect(site_url('Customer'));
    }

	public function accept($id = null){
		$result = $this->M_Customer->accept($id);
		redirect(site_url('RiwayatCustomer'));
	}
}
