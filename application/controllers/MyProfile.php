<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyProfile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // if ($this->session->userdata('authenticated') != true) {
        //     $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
        //     redirect(site_url('Admin'));
        // }
        $this->load->database();
        $this->load->model('M_MyProfile');
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Edit Users - Al Fazza";
		// if(!isset($id)) redirect('Users');
        $data['users'] = $this->M_MyProfile->getById($this->session->userdata('id'));
		if (!$data['users']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('user/profile', $data);
		$this->load->view('templates/footer');
	}

	public function updateData()
	{
        $customer = $this->M_MyProfile;
        $validation = $this->form_validation;

		$ID 	=  $this->input->post('id');
		$data = array(
            'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
            'email' 		=> $this->input->post('email'),
            'username' 		=> $this->input->post('username'),
        );
		
        if ($this->input->post('password') != null) {
            $data['password'] = md5($this->input->post('password'));
        }
        
        $config['upload_path']      = 'assets/upload/users';
		$config['allowed_types']    = 'jpeg|jpg|png|pdf';
        $config['file_name'] 	    = 'profile-'.$this->input->post('username');
		$this->load->library('upload', $config);

		if (!empty($_FILES['file_foto']['name'])) {
			if (!$this->upload->do_upload('file_foto')) {
				echo "upload Gagal".$this->upload->display_errors('', ''); die();
			} else {
				$data['file_foto']	= $this->upload->data('file_name');
			}
		}
		$this->M_MyProfile->update($data, $ID);
		$this->session->set_flashdata('msg', 'Data anda telah di update');
        redirect(site_url('MyProfile'));
	}

}
