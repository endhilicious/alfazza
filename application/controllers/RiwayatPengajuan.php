<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RiwayatPengajuan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('authenticated') != true) {
                $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
                redirect(site_url('Admin'));
        }
		if ($this->session->userdata('status') != 1) {
                $this->session->set_flashdata('msg', 'Anda Telah Logout ! Silahkan Login Terlebih Dahulu');
                redirect(site_url('Dashboard'));
        }
		
        $this->load->database();
        $this->load->model('M_RiwayatPengajuan');
	    $this->load->helper('url');
    }

	public function index()
	{
        $data['title'] = "Riwayat Pengajuan - Al Fazza";
        $data['pengajuan'] = $this->M_RiwayatPengajuan->getAll();
		$this->load->view('templates/header', $data);
		$this->load->view('admin/riwayatPengajuan/index', $data);
		$this->load->view('templates/footer');
	}

	public function add()
	{
        $data['title'] = "Tambah Riwayat Pengajuan - Al Fazza";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/riwayatPengajuan/add');
		$this->load->view('templates/footer');
	}
	
	public function detail($id = null)
	{
        $data['title'] = "Detail Riwayat Pengajuan - Al Fazza";
		if(!isset($id)) redirect('pengajuan');
        $data['pengajuan'] = $this->M_RiwayatPengajuan->getById($id);
		if (!$data['pengajuan']) show_404();
		$this->load->view('templates/header', $data);
		$this->load->view('admin/riwayatPengajuan/detail', $data);
		$this->load->view('templates/footer');
	}

	public function accept($id = null){
        $result = $this->M_RiwayatPengajuan->accept($id);
		redirect(site_url('RiwayatPengajuan'));
    }
	
}
