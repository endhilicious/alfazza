<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                            <h2>PROFILE</h2>
                            <?php if ($this->session->flashdata('msg')) { ?>
                                <div class="alert bg-teal alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    Update Data Berhasil!
                                </div>
                            <?php } ?>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <?= form_open_multipart("MyProfile/updateData"); ?>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="hidden" name="id" value="<?= $users->id ?>">
                                                <input type="text" class="form-control" name="nama_lengkap" required value="<?= $users->nama_lengkap ?>">
                                                <label class="form-label">Nama Lengkap</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="email" required value="<?= $users->email ?>">
                                                <label class="form-label">Email</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required value="<?= $users->username ?>">
                                                <label class="form-label">Username</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="password" class="form-control" name="password">
                                                <label class="form-label">Password (Di isi jika berubah)</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <label class="form-label">Upload Foto (Di isi jika berubah)</label>
                                            <div class="form-line">
                                                <input type="file" class="form-control" name="file_foto">
                                            </div>
                                        </div>
                                        
                                        <button class="btn btn-success waves-effect" type="submit">UPDATE</button>
                                        <button class="btn btn-danger waves-effect" type="reset">RESET</button>
                                    <?= form_close(); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>