<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                            <a href="<?= base_url("Users") ?>" class="btn btn-primary rounded"><i class="material-icons">arrow_back</i>Kembali</a>
                            <h2>EDIT USER</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <?= form_open_multipart("Users/updateData"); ?>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="hidden" name="id" value="<?= $users->id ?>">
                                                <input type="text" class="form-control" name="nama_lengkap" required value="<?= $users->nama_lengkap ?>">
                                                <label class="form-label">Nama Lengkap</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="email" class="form-control" name="email" required value="<?= $users->email ?>">
                                                <label class="form-label">Email</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required pattern="[A-Za-z0-9]+" value="<?= $users->username ?>">
                                                <label class="form-label">Username (Hanya huruf dan angka)</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="password" class="form-control" name="password">
                                                <label class="form-label">Password (Di isi jika berubah)</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <label class="form-label">Upload Foto (Di isi jika berubah)</label>
                                            <div class="form-line">
                                                <input type="file" class="form-control" name="file_foto">
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <label class="form-label">Status</label>
                                            <div class="form-line">
                                                <select name="status" class="form-control" id="">
                                                    <option value=""> -- PILIH -- </option>
                                                    <option value="1" <?php if ($users->status == 1 ) { echo "selected"; } ?>> ADMIN </option>
                                                    <option value="2" <?php if ($users->status == 2 ) { echo "selected"; } ?>> COO </option>
                                                    <option value="3" <?php if ($users->status == 3 ) { echo "selected"; } ?>> CEO </option>
                                                    <option value="4" <?php if ($users->status == 4 ) { echo "selected"; } ?>> SURVEOR </option>
                                                    <option value="5" <?php if ($users->status == 5 ) { echo "selected"; } ?>> SUPER ADMIN </option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <button class="btn btn-success waves-effect" type="submit">ADD</button>
                                        <button class="btn btn-danger waves-effect" type="reset">RESET</button>
                                    <?= form_close(); ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>