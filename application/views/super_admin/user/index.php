<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                        <h2>USER</h2>
                        <a href="<?= base_url()."Users/add" ?>" class="btn btn-success rounded"><i class="material-icons">add</i> Tambah Data</a>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Lengkap</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $no = 1;
                                    foreach($users as $row)
                                    {
                                ?>
                                    <tr>
                                        <td><?= $no ?></td>
                                        <td><?= $row->nama_lengkap ?></td>
                                        <td><?= $row->email ?></td>
                                        <td><?= $row->username ?></td>
                                        <td><?php 
                                                if ($row->status == 1) { echo "ADMIN"; }
                                                elseif ($row->status == 2) { echo "COO"; }
                                                elseif ($row->status == 3) { echo "CEO"; }
                                                elseif ($row->status == 4) { echo "SURVEOR"; }
                                                elseif ($row->status == 5) { echo "SUPER ADMIN"; }
                                            ?></td>
                                        <td>
                                            <a href="<?= base_url("Users/update/".$row->id); ?>" class="btn btn-warning waves-effect m-b-5 m-t-5"> Edit</a>
                                            <a href="<?= base_url("Users/deleteData/".$row->id); ?>" class="confirmation btn btn-danger waves-effect m-b-5 m-t-5"> Hapus</a>
                                        </td>
                                    </tr>
                                <?php $no++; } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>