<section class="content">
    <style>
        h3 { padding-top: 10px; padding-bottom: 10px; }
    </style>
        <div class="container-fluid">
        <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                    <a href="<?= base_url("Survei/detail/".$customer->id) ?>" class="btn btn-primary rounded"><i class="material-icons">arrow_back</i> Kembali</a>
                    <h2>SURVEI <?= $customer->nama_lengkap; ?></h2>
                </div>
                <div class="body">
                <form action="<?= base_url('Survei/updateData') ?>" id="wizard_with_validation" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                        <h3>ALASAN PENGAJUAN</h3>
                        <fieldset>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="hidden" name="id" value="<?= $customer->id ?>">
                                    <input type="text" class="form-control" name="t1_1" value="<?= $survei->t1_1 ?>" required>
                                    <label class="form-label">Siapa yang mau menggunakan barang, dan digunakan untuk apa?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="t1_2" value="<?= $survei->t1_2 ?>" required>
                                    <label class="form-label">Apakah sebelumnya pernah kredit barang di tempat lain?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="t1_3" value="<?= $survei->t1_3 ?>" required>
                                    <label class="form-label">Apakah ada kredit sedang berjalan? Dia dan suaminya</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="t1_4" value="<?= $survei->t1_4 ?>" required>
                                    <label class="form-label">Apa alasan mengjaukan di Al Fazza dari mana?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="t1_5" value="<?= $survei->t1_5 ?>" required>
                                    <label class="form-label">Seberapa paham ia dengan perbedaan kredit syariah dengan kredit konvensional?</label>
                                </div>
                            </div>
                        </fieldset>

                        <h3>KEPASTIAN PEKERJAAN</h3>
                        <fieldset>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t2_1" class="form-control" value="<?= $survei->t2_1 ?>" required>
                                    <label class="form-label">Bekerja disana sudah berapa lama?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t2_2" class="form-control" value="<?= $survei->t2_2 ?>" required>
                                    <label class="form-label">Status kepegawaian apa? Tetap/Kontrak</label>
                                </div>
                            </div>
                            <!-- <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t2_3" class="form-control" value="<?= $survei->t2_3 ?>" required>
                                    <label class="form-label">Bukti ID Card atau SK</label>
                                </div>
                            </div> -->
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t2_4" class="form-control" value="<?= $survei->t2_4 ?>" required>
                                    <label class="form-label">Berapa gajinya?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t2_5" class="form-control" value="<?= $survei->t2_5 ?>" required>
                                    <label class="form-label">Berapa penghasilan pasangan dan kerja dimana?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t2_6" class="form-control" value="<?= $survei->t2_6 ?>" required>
                                    <label class="form-label">Gajian setiap tanggal berapa?</label>
                                </div>
                            </div>
                            <!-- <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t2_7" class="form-control" value="<?= $survei->t2_7 ?>" required>
                                    <label class="form-label">Bukti slip gaji dan mutasi rekening?</label>
                                </div>
                            </div> -->
                            
                        </fieldset>

                        <h3>KEPASTIAN TEMPAT TINGGAL</h3>
                        <fieldset>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t3_1" class="form-control" value="<?= $survei->t3_1 ?>" required>
                                    <label class="form-label">Status tempat tinggal? Milik Pribadi / Milik Orangtua / Keluarga/ Kontrak?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t3_2" class="form-control" value="<?= $survei->t3_2 ?>" required>
                                    <label class="form-label">Tinggal Sudah berapa lama</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t3_3" class="form-control" value="<?= $survei->t3_3 ?>" required>
                                    <label class="form-label">Tinggal bersama siapa</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t3_4" class="form-control" value="<?= $survei->t3_4 ?>" required>
                                    <label class="form-label">Berapa jumlah tanggungan?</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t3_5" class="form-control" value="<?= $survei->t3_5 ?>" required>
                                    <label class="form-label">Cek kesesuaian alamat rumah dengan di KTP?</label>
                                </div>
                            </div>
                            <!-- <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t3_6" class="form-control" value="<?= $survei->t3_6 ?>" required>
                                    <label class="form-label">Bukti Rekening Listrik, KTP, KK</label>
                                </div>
                            </div> -->
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t3_7" class="form-control" value="<?= $survei->t3_7 ?>" required>
                                    <label class="form-label">Tanggapan tetangga (Min. 2 Orang?</label>
                                </div>
                            </div>
                        </fieldset>

                        <h3>JAMINAN DAN PENJAMIN</h3>
                        <fieldset>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t4_1" class="form-control" value="<?= $survei->t4_1 ?>" required>
                                    <label class="form-label">Harga diatas 5 Jt wajib jaminkan BPKB Motor</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="t4_2" class="form-control" value="<?= $survei->t4_2 ?>" required>
                                    <label class="form-label">Siapa yang bisa menjadi penjamin</label>
                                </div>
                            </div>
                        </fieldset>
                        
                        <h3>LAMPIRAN BERKAS</h3>
                        <fieldset>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label">
                                    KTP 
                                    <?php if (isset($customer->file_ktp)) { echo "(Di isi jika berubah) <a target='_blank' href='".base_url("assets/upload/".$customer->file_ktp)."'> Lihat </a>"; } ?>
                                </label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" accept="application/pdf, image/*" class="form-control" name="file_ktp" value="<?= $customer->file_ktp ?>" <?php if (!isset($customer->file_ktp)) { echo "required"; } ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">
                                    Kartu Keluarga
                                    <?php if (isset($customer->file_kk)) { echo "(Di isi jika berubah) <a target='_blank' href='".base_url("assets/upload/".$customer->file_kk)."'> Lihat </a>"; } ?>
                                </label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" accept="application/pdf, image/*" class="form-control" name="file_kk" value="<?= $customer->file_kk ?>" <?php if (!isset($customer->file_kk)) { echo "required"; } ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">
                                    Nomor Rek Listrik
                                    <?php if (isset($customer->rek_listrik)) { echo "(Di isi jika berubah) <a target='_blank' href='".base_url("assets/upload/".$customer->rek_listrik)."'> Lihat </a>"; } ?>
                                </label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" accept="application/pdf, image/*" class="form-control" name="rek_listrik" value="<?= $customer->rek_listrik ?>" <?php if (!isset($customer->rek_listrik)) { echo "required"; } ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">
                                    ID Card Kantor (Jika Ada)
                                    <?php if (isset($customer->id_card_kantor)) { echo "(Di isi jika berubah) <a target='_blank' href='".base_url("assets/upload/".$customer->id_card_kantor)."'> Lihat </a>";} ?>
                                </label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" accept="application/pdf, image/*" class="form-control" name="id_card_kantor" value="<?= $customer->id_card_kantor ?>" <?php if (!isset($customer->id_card_kantor)) { echo "required"; } ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Nomor HP Penjamin</label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="no_hp_penjamin" value="<?= $customer->no_hp_penjamin ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">
                                    KTP Penjamin
                                    <?php if (isset($customer->file_ktp_penjamin)) { echo "(Di isi jika berubah) <a target='_blank' href='".base_url("assets/upload/".$customer->file_ktp_penjamin)."'> Lihat </a>";} ?>
                                </label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" accept="application/pdf, image/*" class="form-control" name="file_ktp_penjamin" value="<?= $customer->file_ktp_penjamin ?>" <?php if (!isset($customer->file_ktp_penjamin)) { echo "required"; } ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">Titik Koordinat (Maps)</label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" accept="application/pdf, image/*" class="form-control" name="maps" value="<?= $customer->maps ?>" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-label">
                                    Foto Survei
                                    <?php if (isset($customer->file_akad)) { echo "(Di isi jika berubah) <a target='_blank' href='".base_url("assets/upload/".$customer->file_akad)."'> Lihat </a>"; } ?>
                                </label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" accept="application/pdf, image/*" class="form-control" name="file_akad" value="<?= $customer->file_akad ?>" <?php if (!isset($customer->file_akad)) { echo "required"; } ?>>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label class="form-label">
                                    Komentar Surveor
                                </label>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                    <textarea name="comment" cols="30" rows="5" class="form-control" required><?= $survei->comment ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btmModal btn btn-success waves-effect">Simpan</button>
                        <button type="reset" class="btn btn-secondary waves-effect" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</section>