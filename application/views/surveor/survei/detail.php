<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                                <a href="<?= base_url("Survei") ?>" class="btn btn-primary rounded"><i class="material-icons">arrow_back</i> Kembali</a>
                                <h2>DETAIL PENGAJUAN</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <h2 align="center" style="display: flex; justify-content: center; gap: 10px; align-items: center;">
                                        <i class="material-icons" style="font-size: 35px">
                                            <?php 
                                                if ($customer->status == 1) {
                                                    echo "verified_user";
                                                } else {
                                                    echo "recent_actors";
                                                }
                                            ?>
                                        </i> 
                                        <?= $customer->nama_lengkap ?>
                                    </h2><br>
                                    <table class="table">
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td><?= $customer->nama_lengkap ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:</td>
                                            <td><?= $customer->alamat ?></td>
                                        </tr>
                                        <tr>
                                            <td>Umur</td>
                                            <td>:</td>
                                            <td><?= $customer->umur ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan</td>
                                            <td>:</td>
                                            <td><?= $customer->pekerjaan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Penghasilan</td>
                                            <td>:</td>
                                            <td><?= $customer->penghasilan ?></td>
                                        </tr>
                                        <tr>
                                            <td>No Handphone</td>
                                            <td>:</td>
                                            <td><?= $customer->no_hp ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama perusahaan tempat bekerja</td>
                                            <td>:</td>
                                            <td><?= $customer->nama_perusahaan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Apakah Pernah Kredit Di tempat lain</td>
                                            <td>:</td>
                                            <td><?= $customer->t1 ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Barang</td>
                                            <td>:</td>
                                            <td><?= $customer->brg_kredit ?></td>
                                        </tr>
                                        <tr>
                                            <td>Merek Barang</td>
                                            <td>:</td>
                                            <td><?= $customer->brg_merk ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tipe Barang</td>
                                            <td>:</td>
                                            <td><?= $customer->brg_type ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tau Alfazza dari mana.?</td>
                                            <td>:</td>
                                            <td><?= $customer->t2 ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alasan mau megajukan kredit di alfazza.?</td>
                                            <td>:</td>
                                            <td><?= $customer->t3 ?></td>
                                        </tr>
                                    </table>
                                    <div class="d-flex justify-content-between" style="display: flex; justify-content: space-between; flex-wrap: wrap;">
                                        <a href="<?= base_url('Survei/survei/'.$customer->id) ?>" class="m-b-5 m-t-5 btn btn-success"><i class="material-icons">check</i> Lakukan Survei</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>