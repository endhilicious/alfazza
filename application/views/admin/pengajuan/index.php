<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                            <h2>PENGAJUAN</h2>
                            <a href="<?= base_url("Pengajuan/add") ?>" class="btn btn-success rounded"><i class="material-icons">add</i> Tambah Data</a>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Pekerjaan</th>
                                            <th>Umur</th>
                                            <th>Perusahaan</th>
                                            <th>No. HP</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if ($pengajuan) {
                                        foreach($pengajuan as $row)
                                        {
                                    ?>
                                        <tr>
                                            <td><?= $row->nama_lengkap ?></td>
                                            <td><?= $row->alamat ?></td>
                                            <td><?= $row->pekerjaan ?></td>
                                            <td><?= $row->umur ?></td>
                                            <td><?= $row->nama_perusahaan ?></td>
                                            <td><?= $row->no_hp ?></td>
                                            <td>
                                                <a href="<?= base_url("Pengajuan/update/".$row->id) ?>" class="btn btn-warning waves-effect m-b-5 m-t-5"> Edit</a>
                                                <a href="<?= base_url("Pengajuan/detail/".$row->id) ?>" class="btn btn-success waves-effect m-b-5 m-t-5"> Detail</a>
                                            </td>
                                        </tr>
                                    <?php } 
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>