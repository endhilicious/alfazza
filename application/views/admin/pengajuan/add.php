<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                                <a href="<?= base_url("Pengajuan") ?>" class="btn btn-primary rounded"><i class="material-icons">arrow_back</i>Kembali</a>
                                <h2>TAMBAH PENGAJUAN</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <form id="form_validation" method="POST" action="<?= base_url('Pengajuan/addData')?>" enctype="multipart/form-data">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama_lengkap" required>
                                                <label class="form-label">Nama Lengkap</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="alamat" required>
                                                        <label class="form-label">Alamat</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="number" class="form-control" name="umur" required>
                                                        <label class="form-label">Umur</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="pekerjaan" required>
                                                <label class="form-label">Pekerjaan</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="penghasilan" required>
                                                <label class="form-label">Penghasilan</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="no_hp" required>
                                                <label class="form-label">No Handphone</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama_perusahaan" required>
                                                <label class="form-label">Nama perusahaan tempat bekerja</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="t1" required>
                                                <label class="form-label">Apakah Pernah Kredit Di tempat lain</label>
                                            </div>
                                        </div>
                                        <h2 class="card-inside-title">Barang yang akan di kredit</h2>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="brg_kredit" required>
                                                <label class="form-label">Jenis Barang</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="brg_merk" required>
                                                <label class="form-label">Merek Barang</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="brg_type" required>
                                                <label class="form-label">Tipe Barang</label>
                                            </div>
                                        </div>

                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="t2" required>
                                                <label class="form-label">Tau Alfazza dari mana.?</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="t3" required>
                                                <label class="form-label">Alasan mau megajukan kredit di alfazza.?</label>
                                            </div>
                                        </div>
                                        <button class="btn btn-success waves-effect" type="submit">ADD</button>
                                        <button class="btn btn-danger waves-effect" type="reset">RESET</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>