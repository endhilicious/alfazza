<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                                <a href="<?= base_url("RiwayatPengajuan") ?>" class="btn btn-primary rounded"><i class="material-icons">arrow_back</i> Kembali</a>
                                <h2>DETAIL PENGAJUAN</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <h2 align="center"><?= $pengajuan->nama_lengkap ?></h2><br>
                                    <table class="table">
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->nama_lengkap ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->alamat ?></td>
                                        </tr>
                                        <tr>
                                            <td>Umur</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->umur ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pekerjaan</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->pekerjaan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Penghasilan</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->penghasilan ?></td>
                                        </tr>
                                        <tr>
                                            <td>No Handphone</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->no_hp ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama perusahaan tempat bekerja</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->nama_perusahaan ?></td>
                                        </tr>
                                        <tr>
                                            <td>Apakah Pernah Kredit Di tempat lain</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->t1 ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Barang</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->brg_kredit ?></td>
                                        </tr>
                                        <tr>
                                            <td>Merek Barang</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->brg_merk ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tipe Barang</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->brg_type ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tau Alfazza dari mana.?</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->t2 ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alasan mau megajukan kredit di alfazza.?</td>
                                            <td>:</td>
                                            <td><?= $pengajuan->t3 ?></td>
                                        </tr>
                                    </table>
                                    <?php if ($pengajuan->status == 0) { ?>
                                        <div class="d-flex justify-content-between" style="display: flex; justify-content: space-between; flex-wrap: wrap;">
                                            <a href="<?= base_url("RiwayatPengajuan/accept/".$pengajuan->id) ?>" class="m-b-5 m-t-5 btn btn-success"><i class="material-icons">check</i> Ajukan Kembali Pengajuan</a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>