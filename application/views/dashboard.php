<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                            <div class="text">Customer Aktif</div>
                            <div class="number"><?= $customer ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Pengajuan</div>
                            <div class="number"><?= $pengajuan ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content">
                            <div class="text">Total Customer</div>
                            <!-- <div class="number count-to" data-from="0" data-to="400000000" data-speed="1000" data-fresh-interval="20"></div> -->
                            <div class="number" ><?= $total_customer ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Total Investor</div>
                            <div class="number count-to" data-from="0" data-to="30" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                CUSTOMER AL-FAZZA
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Pekerjaan</th>
                                            <th>Umur</th>
                                            <th>Perusahaan</th>
                                            <th>No. HP</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach($all as $row)
                                        {
                                    ?>
                                        <tr>
                                            <td><?= $row->nama_lengkap ?></td>
                                            <td><?= $row->alamat ?></td>
                                            <td><?= $row->pekerjaan ?></td>
                                            <td><?= $row->umur." Tahun" ?></td>
                                            <td><?= $row->nama_perusahaan ?></td>
                                            <td><?= $row->no_hp ?></td>
                                            <td><?php 
                                                if ($row->status == 1) {
                                                    echo "<span style='color: orange'>Seleksi Admin</span>";
                                                } else if ($row->status == 2) {
                                                    echo "<span style='color: orange'>Seleksi COO</span>";
                                                } else if ($row->status == 3) {
                                                    echo "<span style='color: orange'>Sedang Survei</span>";
                                                } else if ($row->status == 4) {
                                                    echo "<span style='color: orange'>Telah Survei dan Sedang di review oleh CEO</span>";
                                                } else if ($row->status == 5) {
                                                    echo "<span style='color: green'>Akad</span>";
                                                } else {
                                                    echo "<span style='color: red'>Pengajuan Ditolak</span>";
                                                }
                                                
                                            ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>