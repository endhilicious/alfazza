    <!-- Jquery Core Js -->
    <script src="<?= base_url('assets') ?>/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url('assets') ?>/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>


    <!-- Jquery CountTo Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/jquery-countto/jquery.countTo.js"></script>
    
    <!-- Morris Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/raphael/raphael.min.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/morrisjs/morris.js"></script>
    
    <!-- ChartJs -->
    <script src="<?= base_url('assets') ?>/plugins/chartjs/Chart.bundle.js"></script>
    
    <!-- Custom Js -->
    <script src="<?= base_url('assets') ?>/js/admin.js"></script>
    <script src="<?= base_url('assets') ?>/js/pages/tables/jquery-datatable.js"></script>
    <script src="<?= base_url('assets') ?>/js/pages/index.js"></script>
    <script src="<?= base_url('assets') ?>/js/pages/charts/morris.js"></script>
    <script src="<?= base_url('assets') ?>/js/pages/ui/modals.js"></script>
    <script src="<?= base_url('assets') ?>/js/pages/forms/basic-form-elements.js"></script>
    <script src="<?= base_url('assets') ?>/js/pages/forms/form-wizard.js"></script>
    <!-- Flot Charts Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/flot-charts/jquery.flot.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<?= base_url('assets') ?>/plugins/flot-charts/jquery.flot.time.js"></script>
    <!-- Sweet Alert Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- Sparkline Chart Plugin Js -->
    <script src="<?= base_url('assets') ?>/plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Demo Js -->
    <script src="<?= base_url('assets') ?>/js/demo.js"></script>

    <script type="text/javascript">
        $('.confirmation').on('click', function () {
            return confirm('Are you sure?');
        });
        $('.confirmation-reject').on('click', function () {
            return confirm('Apakah yakin untuk mereject customer?');
        });
    </script>
    <script>
        $('.btnModal').on('click', function () {
            $('#mdModal').modal('show');
        });
        $('.btnModalReject').on('click', function () {
            $('#mdModalReject').modal('show');
        });
    </script>
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "AccountingService",
        "name": "Al Fazza Kredit Syariah",
        "image": "https://alfazzakreditsyariah.com/assets/landingpageassets/img/favicon.png",
        "@id": "https://alfazzakreditsyariah.com/assets/landingpageassets/img/favicon.png",
        "url": "https://alfazzakreditsyariah.com/",
        "telephone": "+6282291515426",
        "priceRange": "2000000",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Jl. Pajjaiang No. 36, Daya",
            "addressLocality": "Makassar",
            "postalCode": "90241",
            "addressCountry": "ID"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": -5.1089366,
            "longitude": 119.5189396
        },
        "openingHoursSpecification": {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
            ],
            "opens": "09:00",
            "closes": "18:00"
        } 
    }
    </script>
</body>

</html>