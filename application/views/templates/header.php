<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $title ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?= base_url("assets/images/logo.png") ?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url('assets') ?>/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url('assets') ?>/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <!-- Sweet Alert Css -->
    <link href="<?= base_url('assets') ?>/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
    <link href="<?= base_url('assets') ?>/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?= base_url('assets') ?>/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?= base_url('assets') ?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<?= base_url('assets') ?>/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= base_url('assets') ?>/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?= base_url('assets') ?>/css/themes/all-themes.css" rel="stylesheet" />
    <style>
        .btn{border-radius: 1rem !important;}
    </style>
</head>

<body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">Al-Fazza Management System</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <?php
                        // if (isset($this->session->userdata["image"]) || $this->session->userdata["image"] != "") {
                        //     $photo_url = "upload/users/".$this->session->userdata['image'];
                        // } else {
                        //     $photo_url = "images/user.png";
                        // }
                        
                        if ($this->session->userdata["image"] == "" ) {
                            $photo_url = "images/user.png";
                        } else {
                            $photo_url = "upload/users/".$this->session->userdata['image'];
                        }
                     ?>
                     <img src="<?= base_url("assets/").$photo_url; ?>" style="max-height: fit-content; border: 1px solid #ffff; width: 50px; height: 50px;" width="50" height="50" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= $this->session->userdata['nama'] ?></div>
                    <div class="email">
                        <?php 
                            if ($this->session->userdata['status'] == 1) {
                                echo "Admin" ;
                            } else if ($this->session->userdata['status'] == 2) {
                                echo "Chief Operasional Officer";
                            } else if ($this->session->userdata['status'] == 3) {
                                echo "Chief Executive Officer";
                            } else if ($this->session->userdata['status'] == 4) {
                                echo "Sureveor";
                            } else if ($this->session->userdata['status'] == 5) {
                                echo "Super Admin";
                            } else {
                                echo "Status tidak ditemukan";
                            }
                        ?>
                    </div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="<?= base_url('MyProfile') ?>"><i class="material-icons">person</i>My Profile</a></li>
                            <li><a href="<?= base_url('Auth/logout') ?>"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li <?php if($this->uri->segment(1)=="Dashboard") {echo "class='active'";} ?> >
                        <a href="<?= base_url('Dashboard') ?>">
                            <i class="material-icons">dashboard</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <?php if ($this->session->userdata['status'] == 1) { ?>

                        <li <?php if($this->uri->segment(1)=="Pengajuan") {echo "class='active'";} ?>>
                            <a href="<?= base_url('Pengajuan') ?>">
                                <i class="material-icons">format_indent_increase</i>
                                <span>Pengajuan</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=="RiwayatCustomer") {echo "class='active'";} ?>>
                            <a href="<?= base_url('RiwayatCustomer') ?>">
                                <i class="material-icons">format_indent_decrease</i>
                                <span>Riwayat Pengajuan</span>
                            </a>
                        </li>

                    <?php } else if ($this->session->userdata['status'] == 2) { ?>

                        <li <?php if($this->uri->segment(1)=="Customer") {echo "class='active'";} ?> >
                            <a href="<?= base_url('Customer') ?>">
                                <i class="material-icons">assignment_turned_in</i>
                                <span>Pengajuan Customer</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=="Onsurvei") {echo "class='active'";} ?> >
                            <a href="<?= base_url('Onsurvei') ?>">
                                <i class="material-icons">youtube_searched_for</i>
                                <span>On Survei</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=="RiwayatCustomer") {echo "class='active'";} ?> >
                            <a href="<?= base_url('RiwayatCustomer') ?>">
                                <i class="material-icons">people_outline</i>
                                <span>Status Customer</span>
                            </a>
                        </li>

                    <?php } else if ($this->session->userdata['status'] == 3) { ?>

                        <li <?php if($this->uri->segment(1)=="Approval") {echo "class='active'";} ?> >
                            <a href="<?= base_url('Approval') ?>">
                                <i class="material-icons">done_all</i>
                                <span>Approval</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=="RiwayatCustomer") {echo "class='active'";} ?> >
                            <a href="<?= base_url('RiwayatCustomer') ?>">
                                <i class="material-icons">history</i>
                                <span>Riwayat Customer</span>
                            </a>
                        </li>

                    <?php } else if ($this->session->userdata['status'] == 4) { ?>

                        <li <?php if($this->uri->segment(1)=="Survei") {echo "class='active'";} ?> >
                            <a href="<?= base_url('Survei') ?>">
                                <i class="material-icons">location_on</i>
                                <span>Survei</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=="RiwayatCustomer") {echo "class='active'";} ?> >
                            <a href="<?= base_url('RiwayatCustomer') ?>">
                                <i class="material-icons">forum</i>
                                <span>Hasil Survei</span>
                            </a>
                        </li>

                    <?php } else if ($this->session->userdata['status'] == 5) { ?>

                        <li <?php if($this->uri->segment(1)=="Profile") {echo "class='active'";} ?> >
                            <a href="<?= base_url('Profile') ?>">
                                <i class="material-icons">domain</i>
                                <span>Profile</span>
                            </a>
                        </li>
                        <li <?php if($this->uri->segment(1)=="Users") {echo "class='active'";} ?> >
                            <a href="<?= base_url('Users') ?>">
                                <i class="material-icons">person</i>
                                <span>Users</span>
                            </a>
                        </li>

                    <?php } ?>
                    <li>
                        <a href="<?= base_url('Auth/logout') ?>">
                            <i class="material-icons">logout</i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
    </section>