<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                            <h2>RIWAYAT CUSTOMER</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Pekerjaan</th>
                                            <th>Umur</th>
                                            <th>Perusahaan</th>
                                            <th>No. HP</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach($customer as $row)
                                        {
                                    ?>
                                        <tr>
                                            <td><?= $row->nama_lengkap ?></td>
                                            <td><?= $row->alamat ?></td>
                                            <td><?= $row->pekerjaan ?></td>
                                            <td><?= $row->umur." Tahun" ?></td>
                                            <td><?= $row->nama_perusahaan ?></td>
                                            <td><?= $row->no_hp ?></td>
                                            <td><?php 
                                                if ($row->status == 1) {
                                                    echo "<span style='color: orange'>Seleksi Admin</span>";
                                                } else if ($row->status == 2) {
                                                    echo "<span style='color: orange'>Seleksi COO</span>";
                                                } else if ($row->status == 3) {
                                                    echo "<span style='color: orange'>Sedang Survei</span>";
                                                } else if ($row->status == 4) {
                                                    echo "<span style='color: orange'>Telah Survei dan Sedang di review oleh CEO</span>";
                                                } else if ($row->status == 5) {
                                                    echo "<span style='color: green'>Akad</span>";
                                                } else {
                                                    echo "<span style='color: red'>Pengajuan Ditolak</span>";
                                                }
                                                
                                            ?></td>
                                            <td>
                                                <a href="<?= base_url("RiwayatCustomer/detail/".$row->id); ?>" class="btn btn-success waves-effect m-b-5 m-t-5"> Detail</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>