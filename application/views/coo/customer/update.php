<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                            <a href="<?= base_url("Customer") ?>" class="btn btn-primary rounded"><i class="material-icons">arrow_back</i>Kembali</a>
                            <h2>TAMBAH CUSTOMER</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <?= form_open_multipart("Customer/updateData"); ?>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="hidden" name="id" required value="<?= $customer->id ?>">
                                            <input type="text" class="form-control" name="nama_lengkap" required value="<?= $customer->nama_lengkap ?>">
                                            <label class="form-label">Nama Lengkap</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="alamat" required value="<?= $customer->alamat ?>">
                                                    <label class="form-label">Alamat</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="number" class="form-control" name="umur" required value="<?= $customer->umur ?>">
                                                    <label class="form-label">Umur</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="pekerjaan" required value="<?= $customer->pekerjaan ?>">
                                            <label class="form-label">Pekerjaan</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="penghasilan" required value="<?= $customer->penghasilan ?>">
                                            <label class="form-label">Penghasilan</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="no_hp" required value="<?= $customer->no_hp ?>">
                                            <label class="form-label">No Handphone</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nama_perusahaan" required value="<?= $customer->nama_perusahaan ?>">
                                            <label class="form-label">Nama perusahaan tempat bekerja</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="t1" required value="<?= $customer->t1 ?>">
                                            <label class="form-label">Apakah Pernah Kredit Di tempat lain</label>
                                        </div>
                                    </div>
                                    <h2 class="card-inside-title">Barang yang akan di kredit</h2>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="brg_kredit" required value="<?= $customer->brg_kredit ?>">
                                            <label class="form-label">Jenis Barang</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="brg_merk" required value="<?= $customer->brg_merk ?>">
                                            <label class="form-label">Merek Barang</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="brg_type" required value="<?= $customer->brg_type ?>">
                                            <label class="form-label">Tipe Barang</label>
                                        </div>
                                    </div>

                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="t2" required value="<?= $customer->t2 ?>">
                                            <label class="form-label">Tau Alfazza dari mana.?</label>
                                        </div>
                                    </div>
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="t3" required value="<?= $customer->t3 ?>">
                                            <label class="form-label">Alasan mau megajukan kredit di alfazza.?</label>
                                        </div>
                                    </div>
                                    <h2 class="card-inside-title">Lampiran</h2>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="form-label">KTP (Di isi jika berubah)</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="file" accept="application/pdf, image/*" class="form-control" name="file_ktp" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">Kartu Keluarga (Di isi jika berubah)</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="file" accept="application/pdf, image/*" class="form-control" name="file_kk" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">Nomor Rek Listrik (Di isi jika berubah)</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="file" accept="application/pdf, image/*" class="form-control" name="rek_listrik"  >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">ID Card Kantor (Jika Ada)</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="file" accept="application/pdf, image/*" class="form-control" name="id_card_kantor">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">Nomor HP Penjamin</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="no_hp_penjamin" required value="<?= $customer->no_hp_penjamin ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">KTP Penjamin (Di isi jika berubah)</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="file" accept="application/pdf, image/*" class="form-control" name="file_ktp_penjamin">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">Titik Koordinat (Maps)</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="maps" required value="<?= $customer->maps ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="form-label">Foto Akad (Di isi jika berubah)</label>
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="file" accept="application/pdf, image/*" class="form-control" name="file_akad">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-success waves-effect" type="submit">ADD</button>
                                    <button class="btn btn-danger waves-effect" type="reset">RESET</button>
                                <?= form_close(); ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>