<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://code.jquery.com/jquery-2.1.0.min.js" ></script>
  <script src="<?= base_url('assets/js/login.js') ?>" ></script>
  <link href="<?= base_url('assets/css/login.css') ?>" rel="stylesheet">
    <!-- Favicon-->
    <link rel="icon" href="<?= base_url("assets/images/logo.png") ?>" type="image/x-icon">
  <title>Alfazza Management System</title>
  <style>
      body{
        background: rgb(101,224,188);
        background: linear-gradient(90deg, rgba(101,224,188,1) 0%, rgba(116,249,140,1) 48%, rgba(0,255,192,1) 100%);
      }
      .alert {
        padding: 10px;
        background-color: #ff9800;;
        color: white;
        max-height: 200px;
        z-index: 999;
        font-size: 15px;
      }
      .closebtn {
        margin-left: 15px;
        color: white;
        font-weight: bold;
        float: right;
        font-size: 22px;
        line-height: 20px;
        cursor: pointer;
        transition: 0.3s;
      }
      .closebtn:hover {
        color: black;
      }
  </style>
</head>
<body>
  
    <?php
      // Cek apakah terdapat session nama message
      if ($this->session->flashdata('message')) { // Jika ada ?>
        <div class="alert">
          <span class="closebtn">&times;</span> 
          <strong>Danger!</strong> 
          <?= $this->session->flashdata('message'); ?>
        </div>
    <?php  } ?>
<div id="formWrapper">
<div id="form">
<div class="logo">
  <img src="<?= base_url('assets/images/logo.png') ?>" alt="logo" width="200" >
</div>
<form action="<?php echo base_url('Auth/login'); ?>" method="post">
    <div class="form-item">
			<p class="formLabel">Email</p>
			<input type="text" name="username" id="email" class="form-style" autocomplete="off"/>
		</div>
		<div class="form-item">
			<p class="formLabel">Password</p>
			<input type="password" name="password" id="password" class="form-style" />
			<p><a href="#" ><small>Forgot Password ?</small></a></p>	
		</div>
		<div class="form-item">
		<input type="submit" class="login pull-right" value="Log In">
		<div class="clear-fix">
      
    </div>
		</div>
</form>
</div>
</div>
</body>
<script>
var close = document.getElementsByClassName("closebtn");
  close.onclick = function(){
    var div = this.parentElement;
    div.style.opacity = "0";
    setTimeout(function(){ div.style.display = "none"; }, 600);
  }
</script>
</html>