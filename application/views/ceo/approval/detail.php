<section class="content">
    <div class="container-fluid">

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                            <a href="<?= base_url("Approval") ?>" class="btn btn-primary rounded"><i class="material-icons">arrow_back</i> Kembali</a>
                            <h2>DETAIL HASIL SURVEI</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <h2 align="center" style="display: flex; justify-content: center; gap: 10px; align-items: center;">
                                    <i class="material-icons" style="font-size: 35px">
                                        <?php 
                                            if ($approval->status == 1) {
                                                echo "verified_user";
                                            } else {
                                                echo "recent_actors";
                                            }
                                        ?>
                                    </i> 
                                    <?= $approval->nama_lengkap ?>
                                </h2><br>
                                <table class="table">
                                    <tr>
                                        <td>Nama</td>
                                        <td>:</td>
                                        <td><?= $approval->nama_lengkap ?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td><?= $approval->alamat ?></td>
                                    </tr>
                                    <tr>
                                        <td>Umur</td>
                                        <td>:</td>
                                        <td><?= $approval->umur ?></td>
                                    </tr>
                                    <tr>
                                        <td>Pekerjaan</td>
                                        <td>:</td>
                                        <td><?= $approval->pekerjaan ?></td>
                                    </tr>
                                    <tr>
                                        <td>Penghasilan</td>
                                        <td>:</td>
                                        <td><?= $approval->penghasilan ?></td>
                                    </tr>
                                    <tr>
                                        <td>No Handphone</td>
                                        <td>:</td>
                                        <td><?= $approval->no_hp ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama perusahaan tempat bekerja</td>
                                        <td>:</td>
                                        <td><?= $approval->nama_perusahaan ?></td>
                                    </tr>
                                    <tr>
                                        <td>Apakah Pernah Kredit Di tempat lain</td>
                                        <td>:</td>
                                        <td><?= $approval->t1 ?></td>
                                    </tr>
                                    <tr>
                                        <td>Jenis Barang</td>
                                        <td>:</td>
                                        <td><?= $approval->brg_kredit ?></td>
                                    </tr>
                                    <tr>
                                        <td>Merek Barang</td>
                                        <td>:</td>
                                        <td><?= $approval->brg_merk ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tipe Barang</td>
                                        <td>:</td>
                                        <td><?= $approval->brg_type ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tau Alfazza dari mana.?</td>
                                        <td>:</td>
                                        <td><?= $approval->t2 ?></td>
                                    </tr>
                                    <tr>
                                        <td>Alasan mau megajukan kredit di alfazza.?</td>
                                        <td>:</td>
                                        <td><?= $approval->t3 ?></td>
                                    </tr>
                                    <?php 
                                        if (
                                            $approval->file_ktp != NULL &&
                                            $approval->file_kk != NULL &&
                                            $approval->rek_listrik != NULL &&
                                            $approval->id_card_kantor != NULL &&
                                            $approval->file_ktp_penjamin != NULL &&
                                            $approval->no_hp_penjamin != NULL &&
                                            $approval->maps != NULL &&
                                            $approval->status != NULL
                                        ) { ?>
                                        <form action="<?= base_url('Approval/push_to_ceo') ?>" method="POST">
                                            <tr>
                                                <td>File KTP</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="<?= base_url('assets/upload/'.$approval->file_ktp) ?>"><?= $approval->file_ktp ?></a></td>
                                            </tr>   
                                            <tr>
                                                <td>File KK</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="<?= base_url('assets/upload/'.$approval->file_kk) ?>"><?= $approval->file_kk ?></a></td>
                                            </tr>   
                                            <tr>
                                                <td>File Rekening Listrik</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="<?= base_url('assets/upload/'.$approval->rek_listrik) ?>"><?= $approval->rek_listrik ?></a></td>
                                            </tr>   
                                            <tr>
                                                <td>File ID Card Kantor</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="<?= base_url('assets/upload/'.$approval->id_card_kantor) ?>"><?= $approval->id_card_kantor ?></a></td>
                                            </tr>
                                            <tr>
                                                <td>File KTP Penjamin</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="<?= base_url('assets/upload/'.$approval->file_ktp_penjamin) ?>"><?= $approval->file_ktp_penjamin ?></a></td>
                                            </tr>   
                                            <tr>
                                                <td>No. HP Penjamin</td>
                                                <td>:</td>
                                                <td><?= $approval->no_hp_penjamin ?></td>
                                            </tr>   
                                            <tr>
                                                <td>Maps</td>
                                                <td>:</td>
                                                <td><a target="_blank" href="<?= $approval->maps ?>"><?= $approval->maps ?></a></td>
                                            </tr>
                                            <tr>
                                                <td>Liat Hasil Survei</td>
                                                <td>:</td>
                                                <td><button type="button" class="btn btnModal btn-primary">Lihat</button></td>
                                            </tr>
                                            <tr>
                                                <td>Komentar COO</td>
                                                <td>:</td>
                                                <td>
                                                <?= $approval->catatan_coo ?>
                                                </td>
                                            </tr>
                                    <!-- disini else -->
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="3" style="text-align: center; color: red; ">Data Belum Terisi</td>
                                                </tr>
                                            <?php } ?>
                                    <!-- Tutup kondisi -->
                                        </table>
                                    <div class="d-flex justify-content-between" style="display: flex; justify-content: space-between; flex-wrap: wrap;">
                                        <button class="m-b-5 m-t-5 btn btn-danger btnModalReject">Reject</button>
                                        <a href="<?= base_url("Approval/accept/".$approval->id) ?>" class="m-b-5 m-t-5 btn btn-success">Accept</a>
                                    </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
<!-- MODAL HASIL SURVEI -->
<div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title h1" id="defaultModalLabel">Hasil Survei</h4>
            </div>
                <div class="modal-body body">
                    <h4>ALASAN PENGAJUAN</h4>
                    <div class="list-group p-l-10">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Siapa yang mau menggunakan barang, dan digunakan untuk apa?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t1_1 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Apakah sebelumnya pernah kredit barang di tempat lain?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t1_2 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Apakah ada kredit sedang berjalan? Dia dan suaminya</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t1_3 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Apa alasan mengjaukan di Al Fazza dari mana?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t1_4 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Seberapa paham ia dengan perbedaan kredit syariah dengan kredit konvensional?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t1_5 ?>
                            </p>
                        </div>
                    </div>
                    <h4>KEPASTIAN PEKERJAAN</h4>
                    <div class="list-group p-l-10">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Bekerja disana sudah berapa lama?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t2_1 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Status kepegawaian apa? Tetap/Kontrak</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t2_2 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Berapa gajinya?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t2_4 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Berapa penghasilan pasangan dan kerja dimana?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t2_5 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Gajian setiap tanggal berapa?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t2_6 ?>
                            </p>
                        </div>
                    </div>
                    <h4>KEPASTIAN TEMPAT TINGGAL</h4>
                    <div class="list-group p-l-10">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Status tempat tinggal? Milik Pribadi / Milik Orangtua / Keluarga/ Kontrak?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t3_1 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Tinggal Sudah berapa lama</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t3_2 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Tinggal bersama siapa</h4>
                            <p class="list-group-item-text">
                                 <?= $resultsurvei->t3_3 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Berapa jumlah tanggungan?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t3_4 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Cek kesesuaian alamat rumah dengan di KTP?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t3_5 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Tanggapan tetangga (Min. 2 Orang?</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t3_7 ?>
                            </p>
                        </div>
                    </div>
                    <h4>JAMINAN & PENJAMIN</h4>
                    <div class="list-group p-l-10">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Harga diatas 5 Jt wajib jaminkan BPKB Motor</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t4_1 ?>
                            </p>
                        </div>
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Siapa yang bisa menjadi penjamin</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->t4_2 ?>
                            </p>
                        </div>
                    </div>
                    <h4>Tanggapan Surveor</h4>
                    <div class="list-group p-l-10">
                        <div class="list-group-item">
                            <h4 class="list-group-item-heading">Tanggapan Surveor</h4>
                            <p class="list-group-item-text">
                                <?= $resultsurvei->comment ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btmModal btn btn-secondary waves-effect" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL REJECT -->
<div class="modal fade" id="mdModalReject" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title h1" id="defaultModalLabel">Hasil Survei</h4>
            </div>
                <form action="<?= base_url("Approval/d_accept") ?>" method="POST">
                <div class="modal-body body">
                    <h4>ALASAN PENOLAKAN PENGAJUAN</h4>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input type="hidden" name="id" value="<?= $approval->id ?>">
                            <input type="text" class="form-control" name="keterangan_reject" required>
                            <label class="form-label">Tulis keterangan</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Batalkan</button>
                    <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                </div>
                </form>
            </form>
        </div>
    </div>
</div>