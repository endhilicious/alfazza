<section class="content">
        <div class="container-fluid">

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="display: flex; justify-content: space-between; align-items: center">
                            <h2>APPROVAL PENGAJUAN</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Pekerjaan</th>
                                            <th>Umur</th>
                                            <th>No. HP</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        foreach($approval as $row)
                                        {
                                    ?>
                                        <tr>
                                            <td><?= $row->nama_lengkap ?></td>
                                            <td><?= $row->alamat ?></td>
                                            <td><?= $row->pekerjaan ?></td>
                                            <td><?= $row->umur." Tahun" ?></td>
                                            <td><?= $row->no_hp ?></td>
                                            <td><?php 
                                                if (
                                                    $row->file_ktp != NULL &&
                                                    $row->file_kk != NULL &&
                                                    $row->rek_listrik != NULL &&
                                                    $row->id_card_kantor != NULL &&
                                                    $row->file_ktp_penjamin != NULL &&
                                                    $row->no_hp_penjamin != NULL &&
                                                    $row->maps != NULL &&
                                                    $row->status != NULL
                                                ) {
                                                    echo "<span style='color: green;'>Data Telah Lengkap</span>";
                                                } else {
                                                    echo "<span style='color: red;'>Data Belum Lengkap</span>";
                                                }?></td>
                                            <td>
                                                <a href="<?= base_url("Approval/detail/".$row->id); ?>" class="btn btn-success waves-effect m-b-5 m-t-5"> Detail</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>