<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Landingpage';
$route['404_override'] = 'Landingpage';
$route['translate_uri_dashes'] = FALSE;
