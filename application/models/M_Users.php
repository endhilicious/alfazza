<?php 

class M_Users extends CI_Model {

        private $_table = "tb_users";

        public function getAll()
        {
                $query = $this->db->get($this->_table);
                return $query->result();
        }
        public function getByID($id){
                return $this->db->get_where($this->_table, ["id" => $id])->row();
        }
        public function insert($data)
        {       
                $this->db->insert($this->_table, $data);
        }
        public function update($data, $id)
        {
                $this->db->update($this->_table, $data, array('id' => $id));
        }
        public function delete($id)
        {
                return $this->db->delete($this->_table, array('id' => $id));
        }
}
