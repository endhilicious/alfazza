<?php 

class M_Pengajuan extends CI_Model {

        private $_table = "tb_customer";

        public function getAll()
        {
                return $this->db->get_where($this->_table, ["status" => 1])->result();
        }

        public function getByID($id){
                return $this->db->get_where($this->_table, ["id" => $id])->row();
        }

        public function insert($data)
        {       
                $this->db->insert($this->_table, $data);
        }

        public function update($data, $id)
        {
                $this->db->update($this->_table, $data, array('id' => $id));
        }

        public function delete($id)
        {
                return $this->db->delete($this->_table, array('id' => $id));
        }
        public function accept($id)
        {
                $query = 'UPDATE tb_customer SET status = status + 1 WHERE id=?';
                $this->db->query($query, $id);
        }
        public function d_accept($id)
        {
                $query = 'UPDATE tb_customer SET status = status - 1 WHERE id=?';
                $this->db->query($query, $id);
        }

}
