
<?php 

class M_Dashboard extends CI_Model {

        private $_table = "tb_customer";

        public function getAll()
        {
                $query = $this->db->get($this->_table);
                return $query->result();
        }

        public function getAllActiveCustomer()
        {
            $this->db->where('status =', 5);
            $query = $this->db->get($this->_table);
            return $query->num_rows();
        }
        
        public function getAllCustomer()
        {
            $query = $this->db->get($this->_table);
            return $query->num_rows();
        }

        public function getAllPengajuan(){
            $this->db->where('status =', 1);
            $query = $this->db->get($this->_table);
            return $query->num_rows();
        }
}
