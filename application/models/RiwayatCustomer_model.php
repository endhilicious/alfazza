<?php 

class RiwayatCustomer_model extends CI_Model {

        private $_table = "tb_riwayat_customer";

        public $id;
        public $nama_lengkap;
        public $alamat;
        public $umur;
        public $pekerjaan;
        public $penghasilan;
        public $no_hp;
        public $nama_perusahaan;
        public $t1;
        public $brg_kredit;
        public $brg_merk;
        public $brg_type;
        public $t2;
        public $t3;
        public $file_ktp;
        public $file_kk;
        public $file_akad;
        public $rek_listrik;
        public $id_card_kantor;
        public $file_ktp_penjamin;
        public $no_hp_penjamin;
        public $maps;
        public $status;

        public function getAll()
        {
                $query = $this->db->get($this->_table);
                return $query->result();
        }

        public function getByID($id){
                return $this->db->get_where($this->_table, ["id" => $id])->row();
        }

        public function insert($data)
        {       
                $this->db->insert($this->_table, $data);
        }

        public function update($data, $id)
        {
                $this->db->update($this->_table, $data, array('id' => $id));
        }

        public function delete($id)
        {
                return $this->db->delete($this->_table, array('id' => $id));
        }

}
