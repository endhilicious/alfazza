<?php 

class M_Survei extends CI_Model {

        private $_table = "tb_survei";

        public function getAll(){
                return $this->db->get_where('tb_customer', ["id_surveor" => $this->session->userdata['id'], "status" => 3])->result();
        }
        public function getByID($id){
                return $this->db->get_where($this->_table, ["id_customer" => $id])->row();
        }
        public function update($data, $id){
                $this->db->update($this->_table, $data, array('id_customer' => $id));
        }

}
