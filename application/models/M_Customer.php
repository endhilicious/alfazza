<?php 

class M_Customer extends CI_Model {

        private $_table = "tb_customer";

        public $id;
        public $nama_lengkap;
        public $alamat;
        public $umur;
        public $pekerjaan;
        public $penghasilan;
        public $no_hp;
        public $nama_perusahaan;
        public $t1;
        public $brg_kredit;
        public $brg_merk;
        public $brg_type;
        public $t2;
        public $t3;
        public $file_ktp;
        public $file_kk;
        public $file_akad;
        public $rek_listrik;
        public $id_card_kantor;
        public $file_ktp_penjamin;
        public $no_hp_penjamin;
        public $maps;
        public $status;

        public function getAll()
        {
                $query = $this->db->get_where($this->_table, ["status" => 2]);
                return $query->result();
        }
        public function getAllApproval()
        {
                $query = $this->db->get_where($this->_table, ["status" => 4]);
                return $query->result();
        }

        public function getAllCustomer()
        {
                $query = $this->db->get($this->_table);
                return $query->result();
        }

        public function getByID($id){
                return $this->db->get_where($this->_table, ["id" => $id])->row();
        }
        
        public function getSurveorByID($id){
                return $this->db->get_where('tb_users', ["id" => $id])->row();
        }

        public function getAllSurveor(){
                return $this->db->get_where('tb_users', ["status" => 4])->result();
        }

        public function getAllSurvei(){
                return $this->db->get_where($this->_table, ["status" => 3])->result();
        }

        public function getResultSurvei($id){
                return $this->db->get_where('tb_survei', ["id_customer" => $id])->row();
        }
        
        public function insert($data)
        {       
                $this->db->insert($this->_table, $data);
        }

        public function update($data, $id)
        {
                $qry = $this->db->update($this->_table, $data, array('id' => $id));
                if ($qry) {
                        echo "success";
                }else {
                        echo $this->db->error_log;
                }
        }

        public function delete($id)
        {
                return $this->db->delete($this->_table, array('id' => $id));
        }
        public function accept($id)
        {
                $query = 'UPDATE tb_customer SET status = status + 1 WHERE id=?';
                $this->db->query($query, $id);
        }
        public function d_accept($id)
        {
                $query = 'UPDATE tb_customer SET status = 0 WHERE id=?';
                $this->db->query($query, $id);
        }
}
