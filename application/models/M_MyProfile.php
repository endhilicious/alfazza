<?php 

class M_MyProfile extends CI_Model {

        private $_table = "tb_users";

        public function getByID($id){
                return $this->db->get_where($this->_table, ["id" => $id])->row();
        }
        public function update($data, $id)
        {
                $this->db->update($this->_table, $data, array('id' => $id));
        }
        public function delete($id)
        {
                return $this->db->delete($this->_table, array('id' => $id));
        }
}
