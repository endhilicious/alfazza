<?php 

class M_RiwayatPengajuan extends CI_Model {

        private $_table = "tb_customer";

        public $id;
        public $nama_lengkap;
        public $alamat;
        public $umur;
        public $pekerjaan;
        public $penghasilan;
        public $no_hp;
        public $nama_perusahaan;
        public $t1;
        public $brg_kredit;
        public $brg_merk;
        public $brg_type;
        public $t2;
        public $t3;
        public $status;

        public function getAll()
        {
                $query = $this->db->get($this->_table);
                return $query->result();
        }

        public function getByID($id){
                return $this->db->get_where($this->_table, ["id" => $id])->row();
        }
        public function accept($id)
        {
                $query = 'UPDATE tb_customer SET status = status + 1 WHERE id=?';
                $this->db->query($query, $id);
        }

}
